import 'package:demo_one/observers/profile_observer.dart';
import 'package:get_it/get_it.dart';

GetIt getIt = GetIt.instance;

void setup() {
  getIt.registerSingleton<ProfileObserver>(ProfileObserver());
}
