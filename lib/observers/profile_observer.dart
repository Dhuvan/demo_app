import 'package:demo_one/models/basket_mode.dart';
import 'package:flutter/widgets.dart';

class ProfileObserver extends ChangeNotifier {
  String name = 'Antony';

  BasetModel basket = BasetModel(id: 0, items: []);

  void changeName({required String newName}) {
    name = newName;
    notifyListeners();
  }
}
