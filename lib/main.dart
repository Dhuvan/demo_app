import 'package:demo_one/observers/locator.dart';
import 'package:demo_one/rout.dart';
import 'package:flutter/material.dart';

import 'screens/lanading_page/landing_page_view.dart';

void main() {
  // WidgetsFlutterBinding.ensureInitialized();
  setup();
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      routes: Routes.routes,
      title: 'Flutter Demo',
      home: const LandingPageView(),
    );
  }
}
