import 'package:demo_one/screens/edit_profile/edit_profile_view.dart';
import 'package:flutter/material.dart';

class Routes {
  Routes._();

  static final routes = <String, WidgetBuilder>{
    EditProfileView.routeName: (context) => const EditProfileView(),
  };
}
