import 'package:demo_one/observers/locator.dart';
import 'package:demo_one/observers/profile_observer.dart';
import 'package:stacked/stacked.dart';

class EditProfileViewModel extends BaseViewModel {
  void changeName() {
    getIt<ProfileObserver>().changeName(newName: 'alexa');
  }
}
