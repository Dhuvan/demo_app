import 'package:demo_one/screens/edit_profile/edit_profile_view_model.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class EditProfileView extends StatelessWidget {
  const EditProfileView({Key? key}) : super(key: key);
  static const routeName = '/editProfileView';

  @override
  Widget build(BuildContext context) => ViewModelBuilder<EditProfileViewModel>.nonReactive(
        viewModelBuilder: () => EditProfileViewModel(),
        builder: (BuildContext context, EditProfileViewModel model, Widget? child) => Scaffold(
          body: Container(),
          floatingActionButton: FloatingActionButton(onPressed: () => model.changeName()),
        ),
      );
}
