import 'package:demo_one/observers/locator.dart';
import 'package:demo_one/observers/profile_observer.dart';
import 'package:demo_one/screens/lanading_page/landing_page_view_model.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:stacked/stacked.dart';

class LandingPageView extends StatelessWidget {
  const LandingPageView({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => ViewModelBuilder<LandingPageViewModel>.nonReactive(
        viewModelBuilder: () => LandingPageViewModel(),
        builder: (BuildContext context, LandingPageViewModel viewModel, Widget? child) => SafeArea(
          child: Scaffold(
            body: Center(
              child: ListView(
                children: const [_ReactiveOne(), _NonReactiveOne()],
              ),
            ),
            floatingActionButton: FloatingActionButton(
              onPressed: () => viewModel.openProfileEdit(context: context),
            ),
          ),
        ),
        onModelReady: (viewModel) => viewModel.inizialize(),
      );
}

class _ReactiveOne extends ViewModelWidget<LandingPageViewModel> {
  const _ReactiveOne({Key? key}) : super(key: key, reactive: true);

  @override
  Widget build(BuildContext context, LandingPageViewModel viewModel) => Text(getIt<ProfileObserver>().name);
}

class _NonReactiveOne extends ViewModelWidget<LandingPageViewModel> {
  const _NonReactiveOne({Key? key}) : super(key: key, reactive: false);
  @override
  Widget build(BuildContext context, LandingPageViewModel viewModel) => ChangeNotifierProvider.value(
      value: getIt<ProfileObserver>(),
      child: Consumer<ProfileObserver>(
        builder: (context, value, child) => Text(value.name),
      ));
}
