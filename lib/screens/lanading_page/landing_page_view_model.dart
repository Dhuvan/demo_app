import 'package:demo_one/screens/edit_profile/edit_profile_view.dart';
import 'package:flutter/material.dart';
import 'package:stacked/stacked.dart';

class LandingPageViewModel extends BaseViewModel {
  String name = 'example';

  void inizialize() {
    name = 'alex';
  }

  void changeName() {
    name = 'Ravi';
    notifyListeners();
  }

  void openProfileEdit({required BuildContext context}) {
    Navigator.pushNamed(context, EditProfileView.routeName);
  }
}
